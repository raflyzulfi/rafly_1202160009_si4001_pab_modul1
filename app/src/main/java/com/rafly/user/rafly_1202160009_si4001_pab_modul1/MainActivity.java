package com.rafly.user.rafly_1202160009_si4001_pab_modul1;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

public class MainActivity extends AppCompatActivity {
    TextView value, satu, dua;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        value = findViewById(R.id.hasil);
    }

    public void cek(View view) {
        try {
            satu = (TextView) findViewById(R.id.satu_atuh);
            dua = (TextView) findViewById(R.id.dua_atuh);
            int a = Integer.valueOf(satu.getText().toString());
            int b = Integer.valueOf(dua.getText().toString());
            int c = a * b;
            if (value != null) {
                value.setText(Integer.toString(c));
            }
        } catch (Exception e) {
            Toast toast =Toast.makeText(this, "Error! "+e, Toast.LENGTH_LONG);
            toast.show();
        }

    }
}
